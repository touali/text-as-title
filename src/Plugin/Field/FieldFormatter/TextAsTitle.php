<?php

namespace Drupal\text_as_title\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'TextAsTitle' formatter.
 *
 * @FieldFormatter(
 *   id = "text_as_title",
 *   label = @Translation("Title"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class TextAsTitle extends StringFormatter
{
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $options['title_level'] = 'h1';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['title_level'] = [
      '#title' => t('Title level'),
      '#type' => 'select',
      '#options' => [
        'h1' => $this->t('Titre 1'),
        'h2' => $this->t('Titre 2'),
        'h3' => $this->t('Titre 3'),
        'h4' => $this->t('Titre 4'),
        'h5' => $this->t('Titre 5'),
        'h6' => $this->t('Titre 6'),
      ],
      '#default_value' => $this->getSetting('title_level')
    ];

    return $element;
  }

  public function settingsSummary() {
    $summary = [];

    $title_level = $this->getSetting('title_level');

    if (isset($title_level)) {
      $summary[] = t('Title level: @level', ['@level' => $title_level]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($elements as &$element) {
      $element['#theme'] = 'text_as_title_formatter';
      $element['#title_level'] = $this->getSetting('title_level');
    }

    return $elements;
  }
}